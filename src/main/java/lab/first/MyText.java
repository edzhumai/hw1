package lab.first;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class MyText implements WritableComparable<MyText>
{
	public final Text s = new Text();

	public MyText(){
	}
	public MyText(String s){
		this.s.set(s);
	}
	public void set(String s){
		this.s.set(s);
	}
    public int compareTo(MyText o) {
        return s.compareTo(o.s);
    }

    public void write(DataOutput dataOutput) throws IOException {
        s.write(dataOutput);
    }

    public void readFields(DataInput dataInput) throws IOException {
        s.readFields(dataInput);
    }
    
    public String toString(){
    	return s.toString();
    }
    
    @Override
    public int hashCode(){
    	return s.hashCode();
    }
    
    @Override
    public boolean equals(final Object obj){
    	if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        final MyText other = (MyText) obj;
        if (s == null) {
            if (other.s != null)
                return false;
        } else if (!s.equals(other.s))
            return false;
        return true;
    }
}