package lab.first;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
//import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;



import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import java.lang.Integer;

public class Lab1 {

	public static class MyMapper
		extends Mapper<Object, Text, MyText, IntWritable>{

	//Find out city ID and bidding price from input splits 
	//and pass to combiner with bid price more than 250
    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
    	String[] split = value.toString().split("	");
    	int biddingPrice = Integer.parseInt(split[19]);
    	
    	if (biddingPrice > 250){
    		context.write(new MyText(split[7]),new IntWritable(1));
    	}
    }
  }
  
  public static class MyCombiner extends Reducer<MyText,IntWritable,MyText,IntWritable>{
	  private IntWritable result = new IntWritable();
	  	//Sum up values and passing them to reducer
	    public void reduce(MyText key, Iterable<IntWritable> values,
	                       Context context
	                       ) throws IOException, InterruptedException {
	      int sum = 0;
	      for (IntWritable val : values) {
	          sum+=val.get();
	      }
	      result.set(sum);
	      key = getCityName(context, key);
	      context.write(key, result);
	    }
	    //Converting city ID to its name from file in distributed cache
	    public MyText getCityName(Context context, MyText cityId) throws IOException{
	    	Configuration conf = context.getConfiguration();
	        FileSystem fs = FileSystem.getLocal(conf);

	        Path[] dataFile = DistributedCache.getLocalCacheFiles(conf);
	        
	        BufferedReader cacheReader = new BufferedReader(new InputStreamReader(fs.open(dataFile[0])));
	        String currentLine;
	        while ((currentLine = cacheReader.readLine()) != null) {
		      	String[] split = currentLine.split("	");
		  		if (split[0].equals(cityId.toString())){
		  			cityId.set(split[1]);
		  			return cityId;
		  		}
	        }
			return cityId;
	    }
  }
  
  public static class MyReducer
       extends Reducer<MyText,IntWritable,Text,IntWritable> {
	  
	private IntWritable result = new IntWritable();
	//Sum up values for keys and writing to output
    public void reduce(MyText key,Iterable<IntWritable> values,
                       Context context
                       ) throws IOException, InterruptedException {
    	int sum = 0;
		for (IntWritable val : values) {
		    sum+=val.get();
		}
		result.set(sum);
    	context.write(new Text(key.toString()), result);
    }    
  }

  //Setting configurations for job, adding file to distributed cache
  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job = Job.getInstance(conf, "Lab1");
    job.setJarByClass(Lab1.class);
    job.setMapperClass(MyMapper.class);
    job.setCombinerClass(MyCombiner.class);
    job.setReducerClass(MyReducer.class);
    job.setMapOutputKeyClass(MyText.class);
    job.setMapOutputValueClass(IntWritable.class);
    DistributedCache.addCacheFile(new Path (args[2]).toUri(), job.getConfiguration());
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    job.setOutputFormatClass(SequenceFileOutputFormat.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    System.exit(job.waitForCompletion(true) ? 0 : 1);
  }
}