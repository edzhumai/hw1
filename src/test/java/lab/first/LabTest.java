package lab.first;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import lab.first.Lab1.MyMapper;
import lab.first.Lab1.MyReducer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class LabTest {
	MapDriver<Object, Text, MyText, IntWritable> mapDriver;
	ReduceDriver<MyText, IntWritable, Text, IntWritable> reduceDriver;
	MapReduceDriver<Object, Text, MyText, IntWritable, Text, IntWritable> mapReduceDriver;
	
	@Before
	public void setUp(){
		MyMapper mapper = new MyMapper();
		MyReducer reducer = new MyReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
	    mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
	}
	
	@Test
	public void testMapper() throws IOException {
	    mapDriver.withInput(new Object(), new Text("dc6c471e922b54a8784c6f721900a14	20131019132200468	1	DAH9cmAszt1	Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari	537.1	157.61.153.*	216	216	2	ac4a2b290ff61f871f526ea3c74b67c9	14392705195e6c7e8d6c84258671d71e	null	1121992714	728	90	OtherView	Na	5	7330	277	82	null	2259	10083"));
	    mapDriver.withOutput(new MyText("216"), new IntWritable(1));
	    mapDriver.runTest();
	}
	
	@Test
	public void testReducer() {
	  List<IntWritable> values = new ArrayList<IntWritable>();
	  values.add(new IntWritable(1));
	  values.add(new IntWritable(1));
	  reduceDriver.withInput(new MyText("foshan"), values);
	  reduceDriver.withOutput(new Text("foshan"), new IntWritable(2));
	  reduceDriver.runTest();
	}
}



